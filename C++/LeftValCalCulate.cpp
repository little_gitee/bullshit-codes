﻿bool TestFunc()
{
    //条件运算时，将作为左值变量放在右边避免出错
    int i = 0;
    int j = 1;
    int k = 2;
    int m = 3;
    int n = 4;
    int nVal = 14;
    //编译通过，但会始终返回false
    if(2 = nVal)
        return false;
    return true;
}